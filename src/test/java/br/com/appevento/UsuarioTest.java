package br.com.appevento;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;

import br.com.appevento.model.Usuario;
import br.com.appevento.repository.RepositorioUsuario;

public class UsuarioTest {
	/*
	@Test
	public void BuscarUsuariosTest() {
		RepositorioUsuario ru = new RepositorioUsuario();
		System.out.println(ru.consultarTodos().toString());
	}
	
	@Test
	public void SalvarUsuarioTest() {
		RepositorioUsuario ru = new RepositorioUsuario();
		@SuppressWarnings("deprecation")
		Date date = new Date(99,11,05);
		Usuario u = new Usuario(
				999999,
				"Flyaway",
				"Mercenária",
				2,
				date);
		assertTrue(ru.salvar(u));
	}
 
	@Test
	public void DeletarUsuarioTest() {
		RepositorioUsuario ru = new RepositorioUsuario();
		assertTrue(ru.deletar(8));
	} */
	
	@Test
	public void AtualizarUsuarioTest() {
		RepositorioUsuario ru = new RepositorioUsuario();
		Date date = new Date(07,05,13);
		Usuario u = new Usuario(
				2055,
				"Inês Brasil",
				"Produtora de Memes",
				10,
				date);
		assertTrue(ru.atualizar(u, 10));
	}
}
