package br.com.appevento;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;

import br.com.appevento.model.Usuario;
import br.com.appevento.modelDAO.UsuarioDAO;

public class UsuarioDAOTeste {

	@Test
	public void InsertUsuarioTest() {
		UsuarioDAO udao = new UsuarioDAO();
		Date date = new Date(90,9,15);
		
		Usuario u = new Usuario(
			2202,
			"Karen Aishila",
			"Secretária Executiva",
			9,
			date
		);
		
		udao.salvarUsuario(u);
	}

}
