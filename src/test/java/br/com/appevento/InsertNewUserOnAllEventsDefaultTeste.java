package br.com.appevento;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.Test;

import br.com.appevento.model.Evento;
import br.com.appevento.model.Usuario;
import br.com.appevento.modelDAO.EventoDAO;
import br.com.appevento.modelDAO.UsuarioDAO;

public class InsertNewUserOnAllEventsDefaultTeste {

	@Test
	public void insertDirectlyTest() {
		Date date = new Date (99,02,12);
		Usuario u = new Usuario(
					44890,
					"Inês Brasil",
					"Cantora",
					0,
					date
				);
		
		UsuarioDAO udao = new UsuarioDAO();
		udao.salvarUsuario(u);
	}

}
