package br.com.appevento;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.Test;

import br.com.appevento.model.Evento;
import br.com.appevento.modelDAO.EventoDAO;

public class InsertAllUserOnNewEventDefaultTest {

	@Test
	public void InsertDirectlyTest() {
		ArrayList<String> lista  = new ArrayList<String>();
		String a = "Caco"; lista.add(a);
		String b = "Jamille";	lista.add(b);
		String c = "Laiza"; lista.add(c);
		
		EventoDAO edao = new EventoDAO();
		Date date = new Date(40,2,10);
		Evento evento = new Evento (
				1,
				"Terceiro Evento",
				lista,
				date,
				"Grindr Rua 9",
				4
		);
		edao.salveEvento(evento);
	}

}
