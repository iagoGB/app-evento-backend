package br.com.appevento.exception;

import java.sql.SQLException;

public class UsuarioNaoEncontradoException extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioNaoEncontradoException(String arg0) {
		super(arg0);

	}

	
}
