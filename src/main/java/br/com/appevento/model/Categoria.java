package br.com.appevento.model;

public class Categoria {
	
	private int categoria_id;
	private String nome;
	
	public int getCategoria_id() {
		return categoria_id;
	}
	
	public void setCategoria_id(int categoria_id) {
		this.categoria_id = categoria_id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "Categoria [categoria_id=" + categoria_id + ", nome=" + nome + "]";
	}
	
}
