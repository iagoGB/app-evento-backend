package br.com.appevento.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventoUsuario {
	
	private String nome; 
	private int evento_id;
	private int usuario_id;
	
	/*@JsonProperty*/
	private boolean isPresent;
	
	public EventoUsuario() {
		
	}
	
	public EventoUsuario(int evento_id, int usuario_id, boolean isPresent) {
		this.evento_id = evento_id;
		this.usuario_id = usuario_id;
		this.isPresent = isPresent;
	}
	
	public EventoUsuario(int evento_id, int usuario_id, boolean isPresent, String nome) {
		this.evento_id = evento_id;
		this.usuario_id = usuario_id;
		this.isPresent = isPresent;
		this.nome = nome;
	}
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getEvento_id() {
		return evento_id;
	}
	
	public void setEvento_id(int evento_id) {
		this.evento_id = evento_id;
	}
	
	public int getUsuario_id() {
		return usuario_id;
	}
	
	public void setUsuario_id(int usuario_id) {
		this.usuario_id = usuario_id;
	}
	
	public boolean isPresent() {
		return isPresent;
	}
	
	public void setPresent(boolean isPresent) {
		this.isPresent = isPresent;
	}
	
	@Override
	public String toString() {
		return "EventoUsuario [evento_id=" + evento_id + ", usuario_id=" + usuario_id + ", isPresent=" + isPresent
				+ "]";
	}
	
	
}
