package br.com.appevento.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.appevento.connection.ConnectionFactory;
import br.com.appevento.exception.UsuarioNaoEncontradoException;
import br.com.appevento.model.Evento;
import br.com.appevento.model.Usuario;
import br.com.appevento.modelDAO.EventoDAO;
import br.com.appevento.modelDAO.UsuarioDAO;

public class RepositorioUsuario  {
	
	
	public RepositorioUsuario() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<Usuario> consultarTodos() {
		ArrayList<Usuario> listaUsuario = new ArrayList<Usuario>();
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM usuario ORDER BY nome ASC";
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();			
			while(rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setUsuario_id(rs.getInt("usuario_id"));
				usuario.setNome(rs.getString("nome"));
				usuario.setCpf(rs.getInt("cpf"));
				usuario.setCarga_horaria(rs.getDouble("carga_horaria"));
				usuario.setProfissao(rs.getString("profissao"));
				usuario.setData_ingresso(rs.getDate("data_ingresso"));
				listaUsuario.add(usuario);
			}
			return listaUsuario;
		} catch (SQLException e) {
			System.err.println("Erro:"+e);
			return null;
		} finally {
			ConnectionFactory.closeConnection(con,stmt,rs);
		}
	}

	public Usuario consultarPorId(Integer id) {
		Usuario u = new Usuario();
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM usuario WHERE usuario_id = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();			
			if (rs.next()) {
				
				u.setUsuario_id(rs.getInt("usuario_id"));
				u.setNome(rs.getString("nome"));
				u.setCpf(rs.getInt("cpf"));
				u.setCarga_horaria(rs.getDouble("carga_horaria"));
				u.setProfissao(rs.getString("profissao"));
				u.setData_ingresso(rs.getDate("data_ingresso"));
			}
			return u;
		} catch (SQLException e) {
			System.err.println("Erro ao pegar usuário "+e);
			return null;
		} finally {
			ConnectionFactory.closeConnection(con,stmt,rs);
		}
	}

	public boolean salvar(Usuario usuario) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String[] returnId = { "usuario_id" };
		try {
			String sql = "INSERT INTO usuario (" + 
					"cpf, nome, profissao, carga_horaria, data_ingresso)" + 
					"VALUES (?, ?, ?, 0, ?)";
			
			stmt = con.prepareStatement(sql, returnId);
				
			stmt.setInt(1, usuario.getCpf());
			stmt.setString(2, usuario.getNome());
			stmt.setString(3, usuario.getProfissao());
			stmt.setDate(4, usuario.getData_ingresso());
			stmt.executeUpdate();
			try{
				//Pegue o id gerado
				rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					usuario.setUsuario_id(rs.getInt(1));
				}
				//Insira esse usuario nos eventos já existentes com presença false
				insereNovoUsuarioEmEventos(usuario.getUsuario_id());
			} catch (Exception e) {
				System.out.println("Erro ao inserir novo usuário:"+ e);
			}
			System.out.println( "Usuario adicionado:" + usuario.toString());
			return true;
		} catch (SQLException e) {
			System.err.println("erro ao inserir usuario:"+ e);
			return false;	
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
	}

	public boolean atualizar(Usuario usuario, Integer id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "UPDATE  usuario SET cpf = ?, nome = ?, profissao = ?, carga_horaria = ?, data_ingresso = ? WHERE usuario_id = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, usuario.getCpf());
			stmt.setString(2, usuario.getNome());
			stmt.setString(3, usuario.getProfissao());
			stmt.setDouble(4, usuario.getCarga_horaria());
			stmt.setDate(5, usuario.getData_ingresso());
			stmt.setInt(6, id);
			stmt.executeUpdate();
			System.out.println("Dados de "+ usuario.getNome() +"  atualizado com sucesso");
			return true;
		} catch (SQLException e) {
			System.err.println("Erro ao atualizar os dados de "+ usuario.getNome()+" Erro: "+ e );
			return false;
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		
	}

	public boolean deletar(Integer id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "DELETE  FROM evento_usuario WHERE usuario_id = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.executeUpdate();
			deletarDaTabelaUsuario(id);
			System.out.println("Usuario com id "+ id+" foi excluido com sucesso da Tabela Usuario_Evento");
			return true;
		} catch (SQLException e) {
			System.err.println("Erro ao deletar usuario do evento de numero "+ id +"  "+ e);
			return false;
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		
	}
	
	public void deletarDaTabelaUsuario(Integer id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "DELETE  FROM usuario WHERE usuario_id = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.executeUpdate();
			System.out.println("Usuario com id "+ id+" foi excluido com sucesso da tabela Usuário");
			
		} catch (SQLException e) {
			System.err.println("Erro ao deletar usuario de numero "+ id +" "+ e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
	}
	
	
	
	public void checaUsuario(int id) throws UsuarioNaoEncontradoException{
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try 
		{	
			String sql = "SELECT * FROM usuario where id = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1,id);
			rs = stmt.executeQuery();
			
		} catch (SQLException e) {
			throw new UsuarioNaoEncontradoException("Usuário com o id "+id+" não foi encontrado");
		}	
	}

	
	
	
	//Insira o usuário recem criado em todos os eventos já existentes com presença false
	public void insereNovoUsuarioEmEventos (int usuarioId){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		EventoDAO edao = new EventoDAO();
		ArrayList<Evento> listaEvento = new ArrayList<Evento>();
		listaEvento = edao.getTodosEventos();
		
		for (Evento ev: listaEvento) {
			
			try {
				String sql = "INSERT INTO evento_usuario (evento_id, usuario_id, presenca) values "
						+ "(?, ?, false)";
				stmt = con.prepareStatement(sql);
				stmt.setInt(1, ev.getEvento_id());
				stmt.setInt(2,usuarioId);
				stmt.executeUpdate();
				System.out.println("Usuario criado "+ usuarioId +" inserido de forma default no evento  "+ev.getEvento_id()+": "+ev.getTitulo());
				
			} catch (SQLException e) {
				System.err.println("Erro ao inserir o novo usuario nos eventos"+ e);
			}
			//Quem o chamou fecha a conexão
		}		
	}
	
	//Insira todos os usuarios com presença - false no evento recem criado JA TA BLZ	
	public void insereUsuariosEmNovoEvento (int eventoId){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		UsuarioDAO udao = new UsuarioDAO();
		ArrayList<Usuario>listaUsuario = new ArrayList<Usuario>();
		listaUsuario = udao.getUsuario();
		
		for (Usuario u: listaUsuario) {
			
			try {
				String sql = "INSERT INTO evento_usuario (evento_id, usuario_id, presenca) VALUES "
						+ "(?, ?, false)";
				stmt = con.prepareStatement(sql);
				stmt.setInt(1, eventoId);
				stmt.setInt(2,u.getUsuario_id());
				stmt.executeUpdate();
				System.out.println("Usuario "+ u.getNome() +" inserido de forma default no evento de id "+eventoId);
				
			} catch (SQLException e) {
				System.err.println("Erro ao inserir usuario em  novo evento criado"+ e);
			}
			//Quem o chamou fecha a conexão
		}	
	}

	
}
