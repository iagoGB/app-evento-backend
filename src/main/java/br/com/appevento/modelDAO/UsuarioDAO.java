package br.com.appevento.modelDAO;

import java.util.ArrayList;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import br.com.appevento.model.Usuario;
import br.com.appevento.repository.RepositorioUsuario;

@RestController
@RequestMapping("/usuarios")
public class UsuarioDAO {
	private final RepositorioUsuario repUsuario = new RepositorioUsuario();
	@GetMapping
	public ArrayList<Usuario> getUsuario(){
		return repUsuario.consultarTodos();		
	}
	
	@GetMapping(value="/{id}")
	public Usuario getUsuarioPorId(@PathVariable Integer id){
		return repUsuario.consultarPorId(id);		
	}
	
	@PostMapping(value="/criar", produces = {MediaType.APPLICATION_JSON_VALUE})
	public boolean salvarUsuario(@RequestBody Usuario usuario) {
		return repUsuario.salvar(usuario);
	}
	
	@PutMapping(value="/editar/{id}",produces = {MediaType.APPLICATION_JSON_VALUE})
	public boolean atualizarUsuario(@RequestBody Usuario usuario, @PathVariable Integer id) {
		return repUsuario.atualizar(usuario,id);
	}
	
	@DeleteMapping(value="/deletar/{id}",produces = {MediaType.APPLICATION_JSON_VALUE})
	public boolean deletarUsuario(@PathVariable Integer id) {
		return repUsuario.deletar(id);
	}
			
			
}
