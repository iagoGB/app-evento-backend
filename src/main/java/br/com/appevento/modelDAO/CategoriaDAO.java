package br.com.appevento.modelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.appevento.model.Categoria;
import br.com.appevento.repository.RepositorioCategoria;

@RestController
@RequestMapping("/categoria")
public class CategoriaDAO {
	
	public ArrayList<Categoria> listaCategoria = new ArrayList<Categoria>(); 
	RepositorioCategoria repCategoria = new RepositorioCategoria();
	
	@GetMapping
	public ArrayList<Categoria> getCategoria(){
		return listaCategoria = repCategoria.consultarTodos();
	}
}
