package br.com.appevento.modelDAO;


import java.util.ArrayList;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.appevento.model.Evento;
import br.com.appevento.repository.RepositorioEvento;

@RestController
@RequestMapping("/eventos")
public class EventoDAO {
	RepositorioEvento repEvento = new RepositorioEvento();
	//Liste todos os eventos
	@GetMapping
	public ArrayList<Evento> getTodosEventos() {
		return repEvento.consultarTodos();
	}
	//Liste um evento único
	@GetMapping(value ="/{id}")
	public  ArrayList<Evento> getEventoPorId(@PathVariable("id") int id) {
		return repEvento.consultarPorId(id);
	}
	
	//Insira um novo evento
	@PostMapping(value="/novo", produces= {MediaType.APPLICATION_JSON_VALUE})
	public boolean salveEvento ( @RequestBody Evento evento ){
		return repEvento.salvar(evento);	
	}
	
	@DeleteMapping(value="/deletar/{id}")
	public boolean deletarEvento (@PathVariable("id") Integer id){
		return repEvento.deletar(id);
	}
	
	
	//Atualize os dados de um evento
	@PutMapping(value="/editar/{id}")
	public void editEvento (
		@RequestBody Evento evento,
		@PathVariable("id") Integer id
	){
		repEvento.atualizar(id, evento);
	}
	
	
}
