package br.com.appevento.modelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.appevento.connection.ConnectionFactory;
import br.com.appevento.model.EventoUsuario;

@RestController
@RequestMapping("usuario-evento")
public class EventoUsuarioDAO {
	@GetMapping(value="/{id}")
	public ArrayList<EventoUsuario> getEventoUsuario(@PathVariable("id") Integer eventoId) {
		
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<EventoUsuario>listaEventoUsuario = new ArrayList<EventoUsuario>();
		
		try {
			String sql = "SELECT nome, u.usuario_id,evento_id, presenca  FROM usuario AS u, evento_usuario AS eu" + 
					" WHERE u.usuario_id = eu.usuario_id AND evento_id = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, eventoId);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				EventoUsuario eu = new EventoUsuario();
				eu.setNome(rs.getString("nome"));
				eu.setPresent(rs.getBoolean("presenca"));
				eu.setUsuario_id(rs.getInt("usuario_id"));
				eu.setEvento_id(rs.getInt("evento_id"));
				
				listaEventoUsuario.add(eu);
				
			}
			return listaEventoUsuario; 
		} catch (SQLException e) {
			System.err.println("Erro ao pegar usuarios do evento "+ eventoId +": "+ e);
			return null;
			
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
			
	}
	
	@PostMapping(value = "/register/{id}",produces =  {MediaType.APPLICATION_JSON_VALUE})
	/* consumes = MediaType.APPLICATION_JSON_VALUE*/
	public ResponseEntity<?> registerPresenceUsuario(
			@PathVariable("id") Integer eventoId, 
			@RequestBody ArrayList<EventoUsuario> eventUsers
	){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		PreparedStatement stmt2 = null;
		PreparedStatement stmt3 = null;
		PreparedStatement stmt4 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		System.out.println(eventUsers);
		
		try {
			
			for(EventoUsuario eu : eventUsers) {
				
				String sql = "UPDATE evento_usuario SET presenca = ? WHERE evento_id = ? AND usuario_id = ? ";
				
				/*String sql = "INSERT INTO evento_usuario (evento_id, usuario_id, presenca) values "
						+ "(?, ?, ?)"; */
				stmt = con.prepareStatement(sql);
				stmt.setBoolean(1, eu.isPresent());
				stmt.setInt(2, eventoId);
				stmt.setInt(3,eu.getUsuario_id());
				stmt.executeUpdate();
				
				if(eu.isPresent()) {
					
					String sql2 = "SELECT carga_horaria FROM usuario WHERE usuario_id = ?";
					
					stmt2 = con.prepareStatement(sql2);
					stmt2.setInt(1,eu.getUsuario_id());
					rs = stmt2.executeQuery();
					
					rs.next();
					
					Double user_carga_horaria = rs.getDouble("carga_horaria");
					
					String sql3 = "SELECT carga_horaria FROM evento WHERE evento_id = ?";
					
					stmt3 = con.prepareStatement(sql3);
					stmt3.setInt(1,eu.getEvento_id());
					rs2 = stmt3.executeQuery();
					
					rs2.next();
					
					Double evento_carga_horaria = rs2.getDouble("carga_horaria");
					
					
					Double user_nova_carga_horaria = user_carga_horaria + evento_carga_horaria;
					
					String sql4 = "UPDATE usuario SET carga_horaria = ? WHERE usuario_id = ?";
					
					stmt4 = con.prepareStatement(sql4);
					stmt4.setDouble(1,user_nova_carga_horaria);
					stmt4.setInt(2,eu.getUsuario_id()); 
					stmt4.executeUpdate();
					
					stmt4.close(); 
					stmt3.close();
					stmt2.close();
					
					
				}
				
			}
			
		} catch (SQLException se) {
			
			System.err.println("Erro ao registrar usuarios do evento "+ eventoId +": "+ se);
			
		} finally {
			
			ConnectionFactory.closeConnection(con, stmt, rs);
			
			
		}
		
		
		return ResponseEntity.ok().build();
		
		
	}
}
